const get = async (url) => {
    try {
        const response = await fetch(url).then(res => res.json());
        if (response.error) {
          console.log(response.error);
          return { response, error: response.error };
        }
        return { response, error: null }
      } catch (error) {
        console.log(error);
        return { response: [], error };
      }
}

export default {
    get
}