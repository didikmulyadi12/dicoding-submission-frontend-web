
import "regenerator-runtime";
import "@lottiefiles/lottie-player";

import "./styles/main.css";
import "./script/component/app-loader"
import "./script/component/app-empty"
import "./script/component/category"
import "./script/component/categories"
import "./script/component/meal"
import "./script/component/meals"
import "./script/component/search-bar"
import "./script/component/meal-detail"
import main from "./script/view/main.js";

document.addEventListener("DOMContentLoaded", main);