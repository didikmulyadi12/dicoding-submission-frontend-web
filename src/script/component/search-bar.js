class SearchBar extends HTMLElement {
  constructor() {
    super();

    this._category = null;
  }

  connectedCallback() {
    this.render();
  }

  set clickEvent(event) {
    this._category = null;
    this._clickEvent = event;
    this.render();
  }

  set clickClearEvent(event) {
    this._category = null;
    this._clickClearEvent = event;
    this.render();
  }

  set category(category) {
    this._category = category;
    this.render();
  }

  get category() {
    return this._category;
  }

  get value() {
    return this.querySelector("#searchInputElement").value;
  }

  render() {
    if (this._category) {
      this.innerHTML = `
                <div class="filtered-category">
                    <p>
                        Filtered by Category : <span>${this._category}</span>
                    </p>
                    <div id="clearButtonElement">Clear</div>
                </div>
            `;
      this.querySelector("#clearButtonElement").addEventListener(
        "click",
        this._clickClearEvent
      );
    } else {
      this.innerHTML = `
                <div id="search-bar">
                    <input id="searchInputElement" placeholder="Search meals" type="text" />
                    <button id="searchButtonElement">Search</button>
                </div>
            `;

      this.querySelector("#searchButtonElement").addEventListener(
        "click",
        this._clickEvent
      );
    }
  }
}

customElements.define("search-bar", SearchBar);
