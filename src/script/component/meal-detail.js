import linkIcon from "../../icons/link-icon.png";
import { getMeals } from "../data/meal";

class MealDetail extends HTMLElement {
  constructor() {
    super();

    this._meal = null;
  }

  connectedCallback() {
    this.render();
  }

  async getMeal(meal) {
    const { response, error } = await getMeals(meal.strMeal);

    this._meal = response.meals.filter(val => val.idMeal === meal.idMeal)[0]

    this.render();
    this.classList.add("open");
  }

  set meal(meal) {

    if(meal.strInstructions) {
        this._meal = meal;

        this.render();
        this.classList.add("open");
    } else {
        this.getMeal(meal)
    }
  }

  closeDetail(thisParam) {
    return () => {
        thisParam.classList.remove("open");
        thisParam.render();
        thisParam._meal = null;
    }
  }

  render() {
    const {
      idMeal,
      strMeal,
      strSource,
      strYoutube,
      strCategory,
      strMealThumb,
      strInstructions,
    } = this._meal || {};

    let ingredients = ``;
    this._meal && Object.keys(this._meal)
      .filter(key => key.includes("strIngredient"))
      .forEach((val, index) => {
        if (this._meal[val] && this._meal[val].trim() !== "") {
          ingredients += `
                <li class="ingredient-item">
                    <p>${this._meal[val]}</p>
                    <span>${this._meal[`strMeasure${index + 1}`]}</span>
                </li>
            `;
        }
      });

    this.innerHTML = `
        <div class="meal-detail" id="meal-detail">
            ${this._meal ? `
                <header style="background-image: url('${strMealThumb}') ">
                    <div>
                        <div>
                            <p>${strCategory}</p>
                            <h2>${strMeal}</h2>
                        </div>
                        <div>
                            <a href="${strSource}" target="_blank">
                                <img src="${linkIcon}" class="white-img" alt="source link">
                            </a>
                            <a href="${strYoutube}" target="_blank">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/71px-YouTube_full-color_icon_%282017%29.svg.png" alt="Youtube link" />
                            </a>
                        </div>
                    </div>
                    <div id="close-detail">x</div>
                </header>
                <div id="meal-detail-content">
                    <p>
                        ${strInstructions}
                    </p>
                    <article>
                        <h3>Ingredients</h3>
                        <ul>
                            ${ingredients}
                        </ul>
                    </article>
                </div>
            `: ''}
        </div>
    `;

    this._meal && this.querySelector("#close-detail").addEventListener("click", this.closeDetail(this));
  }
}

customElements.define("meal-detail", MealDetail);
