import { getMealsByCategory } from "../data/meal";

class Category extends HTMLElement {
  constructor() {
    super();
  }

  set category(category) {
    this._category = category;
    this.render();
  }

  searchMealByCategory(strCategory) {
    return async () => {
      const mealsElement = document.querySelector("meal-list");
      const mealsCategoriesElement = document.querySelector("meal-categories");
      const searchInputElement = document.querySelector("search-bar");

      searchInputElement.category = strCategory;
      mealsElement.isLoading = true;
      mealsElement.updateElement = true;

      const { response, error } = await getMealsByCategory(strCategory);
      mealsElement.isLoading = false;
      mealsElement.meals = (response.meals || []).map(meal => ({
        ...meal,
        strCategory
      }));
      mealsElement.updateElement = true;

      mealsCategoriesElement.updateElement = true;
    };
  }

  render() {
    const searchInputElement = document.querySelector("search-bar");
    const { idCategory, strCategory, strCategoryThumb } = this._category;

    const id = `category-item-${idCategory}`;
    const activeClass =
      searchInputElement.category === strCategory ? " active" : "";

    this.innerHTML = `
        <div class="category-item${activeClass}" id="${id}">
            <h3>${strCategory}</h3>
            <img src="${strCategoryThumb}" alt="${strCategory}" />
        </div>
    `;

    this.querySelector(`#${id}`).addEventListener(
      "click",
      this.searchMealByCategory(strCategory)
    );
  }
}

customElements.define("meal-category", Category);
