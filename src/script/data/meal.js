import api from "../utils/api";

const baseUrl = "https://www.themealdb.com/";
const apiV1 = `${baseUrl}api/json/v1/1/`;

export const getCategories = async () => await api.get(`${apiV1}categories.php`);
export const getIngredients = async () => await api.get(`${apiV1}list.php?i=list`);
export const getSingleRandomMeal = async () => await api.get(`${apiV1}random.php`);
export const getMeals = async name => await api.get(`${apiV1}search.php?s=${name}`);
export const getMealsByCategory = async category => await api.get(`${apiV1}filter.php?c=${category}`);

export const getIngredientUrl = name => `${baseUrl}images/ingredients/${name}.png`;