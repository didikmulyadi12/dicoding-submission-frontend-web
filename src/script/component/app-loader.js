class AppLoader extends HTMLElement {
  constructor() {
    super();

    this.shadowDOM = this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowDOM.innerHTML = `
        <style>
            #loader {
                padding: 1rem;
                display: flex;
                justify-content: center;
            }
        </style>
        <div id="loader">
            <lottie-player
                autoplay loop background="transparent" speed="0.7" style="width: 300px; height: 300px;"
                src="https://assets3.lottiefiles.com/packages/lf20_fSWBhw.json">
            </lottie-player>
        </div>
    `;
  }
}

customElements.define("app-loader", AppLoader);
