import { getMeals } from "../data/meal";

class Meals extends HTMLElement {
  constructor() {
    super();

    this._meals = [];
    this._isLoading = true;
    
  }

  connectedCallback() {
    this.appendChild(document.createElement("app-loader"));
  }

  set meals(meals) {
    this._meals = meals;
  }

  set isLoading(isLoading) {
    this._isLoading = isLoading;
  }

  set updateElement(isUpdate) {
    isUpdate === true && this.render();
  }

  render() {
    this.innerHTML = ``;
    if (this._isLoading) {
      this.appendChild(document.createElement("app-loader"));
    } else if (this._meals && this._meals.length) {
      const div = document.createElement("div");
      div.classList.add("meals");

      this._meals.forEach(meal => {
        const mealItemElement = document.createElement("meal-item");
        mealItemElement.meal = meal;
        div.appendChild(mealItemElement);
      });

      this.appendChild(div);
    } else {
      this.appendChild(document.createElement("app-empty"));
    }
  }
}

customElements.define("meal-list", Meals);
