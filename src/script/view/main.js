import { getMeals, getCategories } from "../data/meal";

const main = async () => {
  const mealsElement = document.querySelector("meal-list");
  const searchInputElement = document.querySelector("search-bar");
  const mealsCategoriesElement = document.querySelector("meal-categories");

  getMeals("").then(({ response }) => {
    mealsElement.isLoading = false;
    mealsElement.meals = response.meals;
    mealsElement.updateElement = true;
  });

  getCategories("").then(({ response }) => {
    mealsCategoriesElement.isLoading = false;
    mealsCategoriesElement.categories = response.categories;
    mealsCategoriesElement.updateElement = true;
  });

  const onClickSearchButton = async () => {
    mealsElement.isLoading = true;
    mealsElement.updateElement = true;

    const { response, error } = await getMeals(searchInputElement.value);

    mealsElement.isLoading = false;
    mealsElement.meals = response.meals;
    mealsElement.updateElement = true;
  };

  const onClickClearButton = async () => {
    const { response, error } = await getMeals("");
    mealsElement.isLoading = false;
    mealsElement.meals = response.meals;
    mealsElement.updateElement = true;

    searchInputElement.clickEvent = onClickSearchButton;
    mealsCategoriesElement.updateElement = true;
  };

  searchInputElement.clickEvent = onClickSearchButton;
  searchInputElement.clickClearEvent = onClickClearButton;
};

export default main;
