import { getCategories } from "../data/meal";

class Categories extends HTMLElement {
  constructor() {
    super();

    this._isLoading = false
    this._categories = []
  }

  connectedCallback() {
    this.appendChild(document.createElement("app-loader"));
  }

  set isLoading(isLoading) {
    this._isLoading = isLoading
  }

  set categories(categories) {
    this._categories = categories
  }

  set updateElement(isUpdate) {
    isUpdate === true && this.render();
  }

  render() {
    this.innerHTML = ``;
    if (this._isLoading) {
      this.appendChild(document.createElement("app-loader"));
    } else if(this._categories && this._categories.length) {
      const div = document.createElement("div")
      div.classList.add("categories")
  
      this._categories.forEach(category => {
          const categoryElement = document.createElement("meal-category");
          categoryElement.category = category
          div.appendChild(categoryElement)
      })
  
      this.appendChild(div)
    } else {
      this.appendChild(document.createElement("app-empty"));
    }
  }

}

customElements.define("meal-categories", Categories);
