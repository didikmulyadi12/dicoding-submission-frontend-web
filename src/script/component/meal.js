class Meal extends HTMLElement {

    constructor() {
        super();
    }

    set meal(meal) {
        this._meal = meal;
        this.render();
    }

    clickMeal(meal) {
        return () => {
            const mealDetailElement = document.querySelector("meal-detail");
            mealDetailElement.meal = meal
        }
    }

    render() {
        const {
            idMeal,
            strMeal,
            strCategory,
            strMealThumb,
        } = this._meal

        const id = `meal-item-${idMeal}`

        this.innerHTML = `
            <article class="meal-item" id="${id}">
                <img src="${strMealThumb}" alt="${strMeal}" />
                <p>${strCategory}</p>
                <h3>${strMeal}</h3>
            </article>
        `;

        this.querySelector(`#${id}`).addEventListener('click', this.clickMeal(this._meal))
    }
}

customElements.define("meal-item", Meal);